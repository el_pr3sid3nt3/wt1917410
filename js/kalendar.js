var trenutniMjesec = 10;
var trenutnaGodina = 2019;

window.onload = function () {
    var datum = new this.Date();

    this.trenutniMjesec = datum.getMonth();
    this.trenutnaGodina = datum.getFullYear();
    if (document.getElementById("kraj") != null) {
        var pocetakDefault = document.getElementById("pocetak");
        var krajDefault = document.getElementById("kraj");
        document.getElementById("sala").value = "VA1";
        pocetakDefault.value = "11:00";
        krajDefault.value = "15:00";

        var kalendarRef = document.getElementById("tabelaKalendar");
        var sala = document.getElementById("sala").value;
        Kalendar.iscrtajKalendar(kalendarRef, this.trenutniMjesec);
        Kalendar.ucitajPodatke(dajPeriodicnaZauzeca(), dajVanrednaZauzeca());
        Kalendar.obojiZauzeca(kalendarRef, this.trenutniMjesec, sala, pocetakDefault.value, krajDefault.value);
    }

    let users = $.ajax({
        type: 'GET',
        url: 'http://localhost:3000/api/osoblje',
        // contentType: 'application/json',
        success: function (data) {
            users = data;
            console.log(data);
            users.forEach(user => {

                var option = document.createElement("option");
                option.text = user.ime + " " + user.prezime;
                option.value = user.id;
                var select = document.getElementById("osoba");
                select.appendChild(option);
//let select = document.getElementById("osoba");
  //              select.appendChild("<option id=\"osobaID\">" + user.ime + " " + user.prezime + "</option>")
            })
        }
    });


}
let Kalendar = (function () {
    let periodicnaZauzeca;
    let vanrednaZauzeca
    let sala; /* 0-01 */
    let pocetak; /* hh:mm */
    let kraj; /* hh:mm */
    let periodicnaRezervacija; /* bool */
    //
    // Ovdje idu privatni atributi
    // 
    function obojiZauzecaImpl(kalendarRef, mjesec, sala, pocetak, kraj) {
        var celijeKalendara = kalendarRef.getElementsByClassName("brojDan");
        var kolikoJePraznihKockica = 0;

        for (let i = 0; i < celijeKalendara.length; i++) {
            if (celijeKalendara[i].children.length != 0) {
                celijeKalendara[i].children[0].setAttribute("class", "slobodna");
            }
        }
        if (this.periodicnaZauzeca != null) {

            for (let i = 0; i < this.periodicnaZauzeca.length; i++) {
                //console.log((this.periodicnaZauzeca[i].semestar == mjesecUSemestru(mjesec)).toString() + "-----" + daLiVrijemeUpadaUDomen(this.periodicnaZauzeca[i].pocetak, this.periodicnaZauzeca[i].kraj, pocetak, kraj).toString());
                if (this.periodicnaZauzeca[i].semestar == mjesecUSemestru(mjesec) && daLiVrijemeUpadaUDomen(this.periodicnaZauzeca[i].pocetak, this.periodicnaZauzeca[i].kraj, pocetak, kraj) && sala == this.periodicnaZauzeca[i].naziv) {
                    //  console.log("desava se");
                    for (let j = this.periodicnaZauzeca[i].dan; j < celijeKalendara.length; j += 7) {
                        //    console.log("da li stvarno?");
                        if (celijeKalendara[j].children.length != 0) {
                            celijeKalendara[j].children[0].classList.remove("slobodna");
                            celijeKalendara[j].children[0].classList.add("zauzeta");
                            celijeKalendara[j].setAttribute("title", this.periodicnaZauzeca[i].pocetak + "<-->" + this.periodicnaZauzeca[i].kraj + "&#010;" + this.periodicnaZauzeca[i].predavac)
                        }
                    }
                }
            }
        }
        for (let i = 0; i < celijeKalendara.length; i++) {
            if (celijeKalendara[i].children.length == 0) kolikoJePraznihKockica++;
            else break;
        }
        if (this.vanrednaZauzeca != null) {
            for (let i = 0; i < this.vanrednaZauzeca.length; i++) {
                try {
                    //console.log("bebe");

                    var danZauzeca = parseInt(this.vanrednaZauzeca[i].datum.substr(0, 2));
                    //console.log("Dan", danZauzeca);
                    var mjesecZauzeca = parseInt(this.vanrednaZauzeca[i].datum.substr(3, 2)) - 1;
                    //console.log("Mjesec", mjesecZauzeca);
                    //console.log(mjesecZauzeca == mjesec, this.vanrednaZauzeca[i].naziv == sala, daLiVrijemeUpadaUDomen(this.vanrednaZauzeca[i].pocetak, this.vanrednaZauzeca[i].kraj, pocetak, kraj));
                    if (mjesecZauzeca == mjesec && this.vanrednaZauzeca[i].naziv == sala && daLiVrijemeUpadaUDomen(this.vanrednaZauzeca[i].pocetak, this.vanrednaZauzeca[i].kraj, pocetak, kraj)) {
                        celijeKalendara[kolikoJePraznihKockica + danZauzeca - 1].children[0].classList.remove("slobodna");
                        celijeKalendara[kolikoJePraznihKockica + danZauzeca - 1].children[0].classList.add("zauzeta");
                    }
                } catch (e) {
                    //console.log(e);
                }
            }
        }
    }

    function obojiPeriodicnaZauzeca() {
        var celijeKalendara = kalendarRef.getElementsByClassName("brojDan");

        var jelIjedno = 0;
        if (this.periodicnaZauzeca != null) {

            for (let i = 0; i < this.periodicnaZauzeca.length; i++) {
                //console.log((this.periodicnaZauzeca[i].semestar == mjesecUSemestru(mjesec)).toString() + "-----" + daLiVrijemeUpadaUDomen(this.periodicnaZauzeca[i].pocetak, this.periodicnaZauzeca[i].kraj, pocetak, kraj).toString());
                if (this.periodicnaZauzeca[i].semestar == mjesecUSemestru(mjesec) && daLiVrijemeUpadaUDomen(this.periodicnaZauzeca[i].pocetak, this.periodicnaZauzeca[i].kraj, pocetak, kraj) && sala == this.periodicnaZauzeca[i].naziv) {
                    jelIjedno++;
                    //console.log("desava se");
                    for (let j = this.periodicnaZauzeca[i].dan; j < celijeKalendara.length; j += 7) {
                        // console.log("da li stvarno?");
                        if (celijeKalendara[j].children.length != 0) {
                            celijeKalendara[j].children[0].classList.remove("slobodna");
                            celijeKalendara[j].children[0].classList.add("zauzeta");
                            celijeKalendara[j].setAttribute("title", this.periodicnaZauzeca[i].pocetak + "<-->" + this.periodicnaZauzeca[i].kraj + "&#010;" + this.periodicnaZauzeca[i].predavac)
                        }
                    }
                }
            }
        }
        return jelIjedno;
    }

    function ucitajPodatkeImpl(periodicna, vanredna) {
        this.periodicnaZauzeca = periodicna;
        this.vanrednaZauzeca = vanredna;
    }

    function iscrtajKalendarImpl(kalendarRef, mjesec) {
        kalendarRef = document.getElementById("tabelaKalendar");
        kalendarRef.innerHTML = "";

        // Naziv mjeseca
        iscrtajNazivKalendara(kalendarRef, mjesec);

        var tbody = document.createElement("tbody");
        kalendarRef.appendChild(tbody);
        // Kockice nazivi dana
        iscrtajNaziveDanaKalendara(tbody);

        // Kockice dani
        iscrtajDaneKalendara(tbody, mjesec);
    }

    function iscrtajNazivKalendara(kalendarRef, mjesec) {

        var caption = document.createElement("caption");
        caption.setAttribute("id", "naslovTabele");
        caption.innerText = dajNazivMjeseca(mjesec);

        var trNaslov = document.createElement("tr");
        trNaslov.setAttribute("id", "redNaslovTabele");

        var thNaslov = document.createElement("th");
        thNaslov.setAttribute("class", "mjesec");
        thNaslov.setAttribute("colspan", "7");
        thNaslov.innerText = dajNazivMjeseca(mjesec);

        trNaslov.appendChild(thNaslov);

        kalendarRef.appendChild(caption);
        kalendarRef.appendChild(trNaslov);

    }

    function iscrtajNaziveDanaKalendara(kalendarRef) {

        var trDani = document.createElement("tr");
        for (let i = 1; i < 8; i++) {
            var tdDan = document.createElement("td");
            tdDan.setAttribute("class", "dan");
            if (i == 1) tdDan.innerText = "PON";
            else if (i == 2) tdDan.innerText = "UTO";
            else if (i == 3) tdDan.innerText = "SRI";
            else if (i == 4) tdDan.innerText = "ČET";
            else if (i == 5) tdDan.innerText = "PET";
            else if (i == 6) tdDan.innerText = "SUB";
            else if (i == 7) tdDan.innerText = "NED";
            else;
            trDani.appendChild(tdDan);
        }
        kalendarRef.appendChild(trDani);
    }

    function iscrtajDaneKalendara(kalendarRef, mjesec) {
        //kalendarRef = document.getElementById("tabelaKalendar");

        /* Iscrtaj prvu sedmicu */
        var sedmica1 = document.createElement("tr");
        sedmica1.setAttribute("id", "sedmica1");
        let i = 1;

        for (i = 1; i < dajPrviDanMjeseca(mjesec, 2019); i++) {
            var dan = document.createElement("td");
            dan.setAttribute("class", "brojDan");
            dan.innerHTML == "&nbsp;";
            sedmica1.appendChild(dan);
        }
        var brojacDana;

        for (i; i < 8; i++) {
            var dan = document.createElement("td");
            dan.setAttribute("class", "brojDan");
            dan.setAttribute("id", (i - dajPrviDanMjeseca(mjesec, 2019) + 1).toString());
            dan.innerText = i - dajPrviDanMjeseca(mjesec, 2019) + 1;

            var div = document.createElement("div");
            div.classList.add("slobodna");
            div.innerHTML = "&nbsp;";

            dan.appendChild(div);
            sedmica1.appendChild(dan);

            brojacDana = i - dajPrviDanMjeseca(mjesec, 2019) + 1;
        }
        brojacDana++;
        kalendarRef.appendChild(sedmica1);
        var brojSedmice = 2;

        while (brojacDana < dajDaneMjeseca(mjesec)) {
            var sedmica = document.createElement("tr");
            sedmica.setAttribute("id", "sedmica" + brojSedmice.toString());
            let j = 1;
            for (j; j < 8; j++) {
                var dan = document.createElement("td");
                dan.setAttribute("class", "brojDan");
                dan.setAttribute("id", brojacDana.toString());
                dan.innerText = brojacDana;

                var div = document.createElement("div");
                div.classList.add("slobodna");
                div.innerHTML = "&nbsp;";

                dan.appendChild(div);
                sedmica.appendChild(dan);
                brojacDana++;
                if (brojacDana > dajDaneMjeseca(mjesec)) {
                    j++;
                    break;
                }
            }
            for (j; j < 8; j++) {
                var dan = document.createElement("td");
                dan.setAttribute("class", "brojDan");
                dan.innerHTML == "&nbsp;";
                sedmica.appendChild(dan);
            }
            kalendarRef.appendChild(sedmica);
        }
    }

    function jelPrestupna(year) {
        return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
    }

    function dajDaneMjeseca(mjesec, godina) {
        if (mjesec == 0 || mjesec == 2 || mjesec == 4 || mjesec == 6 || mjesec == 7 || mjesec == 9 || mjesec == 11) return 31;
        else if (mjesec == 3 || mjesec == 5 || mjesec == 8 || mjesec == 10) return 30;
        else if (mjesec == 1 && jelPrestupna(godina)) return 29;
        else if (mjesec == 1 && !jelPrestupna(godina)) return 28;
    }

    function dajPrviDanMjeseca(mjesec, godina) {
        const now = new Date();


        var k = new Date(godina, mjesec, 1);
        if (k.getDay() == 0) return 7;
        else return k.getDay();
    }

    function dajNazivMjeseca(mjesec) {
        if (mjesec == 0) return "Januar";
        if (mjesec == 1) return "Februar";
        if (mjesec == 2) return "Mart";
        if (mjesec == 3) return "April";
        if (mjesec == 4) return "Maj";
        if (mjesec == 5) return "Juni";
        if (mjesec == 6) return "Juli";
        if (mjesec == 7) return "August";
        if (mjesec == 8) return "Septembar";
        if (mjesec == 9) return "Oktobar";
        if (mjesec == 10) return "Novembar";
        if (mjesec == 11) return "Decembar";
    }

    function mjesecUSemestru(mjesec) {
        if (mjesec == 0 || mjesec == 9 || mjesec == 10 || mjesec == 11) return "zimski";
        else if (mjesec == 1 || mjesec == 2 || mjesec == 3 || mjesec == 4 || mjesec == 5) return "ljetni";
        else return "greska"
    }

    function daLiVrijemeUpadaUDomen(pocetakZauzeca, krajZauzeca, pocetakInput, krajInput) {
        //console.log(pocetakInput + "<-->" + krajInput + "    " + pocetakZauzeca + "<-->" + krajZauzeca);
        if (pocetakInput > krajInput) return false;

        if (pocetakInput <= pocetakZauzeca && krajInput >= krajZauzeca) return true;
        else if (pocetakInput >= pocetakZauzeca && pocetakInput <= krajZauzeca) return true;
        else if (pocetakZauzeca <= krajInput && krajZauzeca >= krajInput) return true;
        else return false;
        /*
        if ((uporediVremena(pocetakZauzeca, pocetakInput) && uporediVremena(krajZauzeca, pocetakInput)) || (uporediVremena(krajInput, pocetakZauzeca) && uporediVremena(krajInput, krajZauzeca))) return false;
        else return true;*/
    }

    function uporediVremena(a, b) {
        return new Date('1/1/1999 ' + a + ':00') <= new Date('1/1/1999 ' + b + ':00');
    }
    return {
        obojiZauzeca: obojiZauzecaImpl,
        ucitajPodatke: ucitajPodatkeImpl,
        iscrtajKalendar: iscrtajKalendarImpl
    }
}());

function promijenjeneVrijednostiForme() {
    try {
        Kalendar.iscrtajKalendar(document.getElementById("tabelaKalendar"), trenutniMjesec);
        Kalendar.ucitajPodatke(dajPeriodicnaZauzeca(), dajVanrednaZauzeca());
        Kalendar.obojiZauzeca(document.getElementById("tabelaKalendar"), trenutniMjesec, document.getElementById("sala").value, document.getElementById("pocetak").value, document.getElementById("kraj").value);
        document.getElementById("osoba").value;
    } catch (e) {
        console.log(e);
    }
}





function prethodniMjesec(referencaNaButton) {
    trenutniMjesec--;
    /*if (trenutniMjesec == -1) {
        trenutniMjesec = 11;
        trenutnaGodina--;
    }*/
    if (trenutniMjesec == 0) {
        referencaNaButton.disabled = true;
    }
    if (trenutniMjesec == 10) {
        document.getElementById("sljedeciButton").disabled = false;
    }
    Kalendar.iscrtajKalendar(document.getElementById("tabelaKalendar"), trenutniMjesec);
    Kalendar.ucitajPodatke(dajPeriodicnaZauzeca(), dajVanrednaZauzeca());
    Kalendar.obojiZauzeca(document.getElementById("tabelaKalendar"), trenutniMjesec, document.getElementById("sala").value, document.getElementById("pocetak").value, document.getElementById("kraj").value);
}

function sljedeciMjesec(referencaNaButton) {
    trenutniMjesec++;
    if (trenutniMjesec == 11) {
        referencaNaButton.disabled = true;
    }
    if (trenutniMjesec == 1) {
        document.getElementById("prethodniButton").disabled = false;
    }
    Kalendar.iscrtajKalendar(document.getElementById("tabelaKalendar"), trenutniMjesec);
    Kalendar.ucitajPodatke(dajPeriodicnaZauzeca(), dajVanrednaZauzeca());
    Kalendar.obojiZauzeca(document.getElementById("tabelaKalendar"), trenutniMjesec, document.getElementById("sala").value, document.getElementById("pocetak").value, document.getElementById("kraj").value);
}