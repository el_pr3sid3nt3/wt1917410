function dajVanrednaZauzeca() {
    let vanredna = [{
            datum: "12.11.2019",
            pocetak: "10:00",
            kraj: "11:00",
            naziv: "VA1",
            predavac: "Nedim Memisevic"
        },
        {
            datum: "17.11.2019",
            pocetak: "11:00",
            kraj: "12:00",
            naziv: "VA1",
            predavac: "Nedim Memisevic"
        },
        {
            datum: "19.11.2019",
            pocetak: "12:00",
            kraj: "13:00",
            naziv: "VA1",
            predavac: "Nedim Memisevic"
        },
        {
            datum: "29.11.2019",
            pocetak: "13:00",
            kraj: "14:00",
            naziv: "VA1",
            predavac: "Nedim Memisevic"
        },
        {
            datum: "09.11.2019",
            pocetak: "14:00",
            kraj: "15:00",
            naziv: "VA1",
            predavac: "Nedim Memisevic"
        }
    ]
    return vanredna;
}

function dajPeriodicnaZauzeca() {
    let periodicna = [{
            dan: 3,
            semestar: "zimski",
            pocetak: "10:00",
            kraj: "11:00",
            naziv: "VA1",
            predavac: "Nedim Memisevic"
        },
        {
            dan: 3,
            semestar: "zimski",
            pocetak: "11:00",
            kraj: "12:00",
            naziv: "VA1",
            predavac: "Nedim Memisevicc"
        },
        {
            dan: 4,
            semestar: "zimski",
            pocetak: "12:00",
            kraj: "13:00",
            naziv: "VA1",
            predavac: "Nedim Memisevicc"
        },
        {
            dan: 4,
            semestar: "zimski",
            pocetak: "13:00",
            kraj: "14:00",
            naziv: "VA1",
            predavac: "Nedim Memisevic"
        },
        {
            dan: 5,
            semestar: "zimski",
            pocetak: "14:00",
            kraj: "15:00",
            naziv: "VA1",
            predavac: "Nedim Memisevic"
        }
    ]
    return periodicna;
}