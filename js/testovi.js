let assert = chai.assert;
describe('Iscrtavanje kalendara', function() {
    //test1 - januar
    it('mjesec sa 31 dana: ocekivano 31 dan', function() {

        let kalRef = document.getElementById("tabelaKalendar");

        Kalendar.iscrtajKalendar(kalRef, 0);
        let dani = kalRef.getElementsByClassName("brojDan");

        var brojKockica = 0;
        for (let i = 0; i < dani.length; i++) {
            if (dani[i].children.length != 0) brojKockica++;
        }
        assert.equal(brojKockica, 31, "Broj dana treba biti 31");
    });
    //test2 - april

    it('mjesec sa 30 dana: ocekivano 30 dana', function() {

        let kalRef = document.getElementById("tabelaKalendar");

        Kalendar.iscrtajKalendar(kalRef, 3);
        let dani = kalRef.getElementsByClassName("brojDan");

        var brojKockica = 0;
        for (let i = 0; i < dani.length; i++) {
            if (dani[i].children.length != 0) brojKockica++;
        }
        assert.equal(brojKockica, 30, "Broj dana treba biti 30");
    });
    //test1 - novembar prvi dan mora biti petak
    it('01. novembar: Prvi dan mora biti petak', function() {

        let kalRef = document.getElementById("tabelaKalendar");

        Kalendar.iscrtajKalendar(kalRef, 10);
        let dani = kalRef.getElementsByClassName("brojDan");
        let brojKockicaPrijePrvog = 0;
        for (let i = 0; i < dani.length; i++) {
            if (dani[i].children.length == 0) brojKockicaPrijePrvog++;
            if (dani[i].children.length != 0) break;
        }
        assert.equal(brojKockicaPrijePrvog, 4, "Broj dana treba biti 31");
    });
    //test1 - novembar prvi dan mora biti petak
    it('30. novembar: Mora biti Subota', function() {

        let kalRef = document.getElementById("tabelaKalendar");

        Kalendar.iscrtajKalendar(kalRef, 10);
        let dani = kalRef.getElementsByClassName("brojDan");
        let brojKockicaPoslijePosljednjeg = 0;
        let hajdeSad = false;
        for (let i = 10; i < dani.length; i++) {
            if (dani[i].children.length == 0) brojKockicaPoslijePosljednjeg++;
        }
        assert.equal(brojKockicaPoslijePosljednjeg, 1, "Broj dana treba biti 31");
    });
    //test za januar da brojevi dana idu od 1 do 31
    it('Dani januara od utorka: Pocinju u utorak zavrsavaju u cetvrtak', function() {

        let kalRef = document.getElementById("tabelaKalendar");

        Kalendar.iscrtajKalendar(kalRef, 0);
        let dani = kalRef.getElementsByClassName("brojDan");
        let brojKockicaPoslijePosljednjeg = 0;

        var stringic = "";
        for (let i = 0; i < dani.length; i++) {
            if (dani[i].children.length != 0) stringic += dani[i].innerText - "\n";
            else stringic += "p";
        }
        assert.equal(stringic, "p12345678910111213141516171819202122232425262728293031ppp", "Broj dana treba biti 31");
    });
});
describe('Bojenje kalendara', function() {
    //test1 - januar
    it('Nema ucitanih podataka: Nista ne treba biti obojeno', function() {

        let kalRef = document.getElementById("tabelaKalendar");

        Kalendar.iscrtajKalendar(kalRef, 10);
        Kalendar.obojiZauzeca(kalRef, 10, "0-01", "12:45", "13:00");
        let dani = kalRef.getElementsByClassName("brojDan");

        var imaLiIstaObojeno = false;
        for (let i = 0; i < dani.length; i++) {
            if (dani[i].children.length > 1) {
                jelImaIstaObojeno = true;
            }
        }
        assert.equal(imaLiIstaObojeno, false, "Nakon što nisu učitani podaci ne treba biti ništa obojeno");
    });

    it('Nema ucitanih podataka: Nista ne treba biti obojeno', function() {

        let kalRef = document.getElementById("tabelaKalendar");

        Kalendar.iscrtajKalendar(kalRef, 10);
        Kalendar.obojiZauzeca(kalRef, 10, "0-01", "12:45", "13:00");
        let dani = kalRef.getElementsByClassName("brojDan");

        var imaLiIstaObojeno = false;
        for (let i = 0; i < dani.length; i++) {
            if (dani[i].children.length > 1) {
                jelImaIstaObojeno = true;
            }
        }
        assert.equal(imaLiIstaObojeno, false, "Nakon što nisu učitani podaci ne treba biti ništa obojeno");
    });
    it('Podaci za isti dan: Treba svejedno biti obojeno', function() {
        // Dva vanredna za 12. novembar
        let kalRef = document.getElementById("tabelaKalendar");

        Kalendar.iscrtajKalendar(kalRef, 10);
        Kalendar.ucitajPodatke(test2Periodicna(), test2Vanredna());
        Kalendar.obojiZauzeca(kalRef, 10, "VA1", "08:00", "17:00");
        let dani = kalRef.getElementsByClassName("brojDan");

        var obojeno = false;
        for (let i = 0; i < dani.length; i++) {
            if (dani[i].id == 12) {
                if (dani[i].children[0].classList.length != 0) {
                    if (dani[i].children[0].classList[0] == "zauzeta")
                        obojeno = true;
                }
            }
        }
        assert.equal(obojeno, true, "Sa podacima za isti dan mora svejedno biti obojena sala");
    });
    it('Podatak za ljetni semestar: Ne treba biti obojen u zimskom', function() {
        // Dva vanredna za 12. novembar
        let kalRef = document.getElementById("tabelaKalendar");

        Kalendar.iscrtajKalendar(kalRef, 10);
        Kalendar.ucitajPodatke(test3Periodicna(), test3Vanredna());
        Kalendar.obojiZauzeca(kalRef, 10, "VA1", "08:00", "17:00");
        let dani = kalRef.getElementsByClassName("brojDan");

        var obojeno = false;
        for (let i = 1; i < dani.length; i += 7) {
            if (dani[i].children.length != 0) {
                if (dani[i].children[0].classList.length != 0) {
                    if (dani[i].children[0].classList[0] == "zauzeta")
                        obojeno = true;
                }
            }
        }
        assert.equal(obojeno, false, "Sa podacima za isti dan mora svejedno biti obojena sala");
    });
    it('Vanredno u novembru, a iscrtavanje Decembra. Ne treba obojiti', function() {

        let kalRef = document.getElementById("tabelaKalendar");

        Kalendar.iscrtajKalendar(kalRef, 10);
        Kalendar.ucitajPodatke(test2Periodicna(), test2Vanredna());
        Kalendar.obojiZauzeca(kalRef, 11, "VA1", "08:00", "17:00");
        let dani = kalRef.getElementsByClassName("brojDan");

        var obojeno = false;
        for (let i = 0; i < dani.length; i++) {
            if (dani[i].id == 12) {
                if (dani[i].children[0].classList.length != 0) {
                    if (dani[i].children[0].classList[0] == "zauzeta")
                        obojeno = true;
                }
            }
        }
        assert.equal(obojeno, false, "Sa podacima za isti dan mora svejedno biti obojena sala");
    });
    it('Dva puta pozivanje oboji zauzece: Ocekivano isto bojenje', function() {

        let kalRef = document.getElementById("tabelaKalendar");

        Kalendar.iscrtajKalendar(kalRef, 10);
        Kalendar.ucitajPodatke(test2Periodicna(), test2Vanredna());
        Kalendar.obojiZauzeca(kalRef, 10, "VA1", "08:00", "17:00");

        var stringPrvi = "";
        let dani = kalRef.getElementsByClassName("brojDan");

        var obojeno = false;
        for (let i = 0; i < dani.length; i++) {
            if (dani[i].children.length != 0) {
                if (dani[i].children[0].classList.length != 0) {
                    if (dani[i].children[0].classList[0] == "zauzeta")
                        stringPrvi += "z";
                    else stringPrvi += "s";
                }
            }
        }
        var stringDrugi = "";
        Kalendar.obojiZauzeca(kalRef, 10, "VA1", "08:00", "17:00");
        for (let i = 0; i < dani.length; i++) {
            if (dani[i].children.length != 0) {
                if (dani[i].children[0].classList.length != 0) {
                    if (dani[i].children[0].classList[0] == "zauzeta")
                        stringDrugi += "z";
                    else stringDrugi += "s";
                }
            }
        }
        assert.equal(stringPrvi, stringDrugi, "Sa podacima za isti dan mora svejedno biti obojena sala");
    });
    it('Razliciti podaci: Ocekivano razlicito bojenje', function() {

        let kalRef = document.getElementById("tabelaKalendar");

        Kalendar.iscrtajKalendar(kalRef, 10);
        Kalendar.ucitajPodatke(test2Periodicna(), test2Vanredna());
        Kalendar.obojiZauzeca(kalRef, 10, "VA1", "08:00", "17:00");

        var stringPrvi = "";
        let dani = kalRef.getElementsByClassName("brojDan");

        var obojeno = false;
        for (let i = 0; i < dani.length; i++) {
            if (dani[i].children.length != 0) {
                if (dani[i].children[0].classList.length != 0) {
                    if (dani[i].children[0].classList[0] == "zauzeta")
                        stringPrvi += "z";
                    else stringPrvi += "s";
                }
            }
        }
        var stringDrugi = "";
        Kalendar.ucitajPodatke(test3Periodicna(), test3Vanredna());
        Kalendar.obojiZauzeca(kalRef, 10, "VA1", "08:00", "17:00");
        for (let i = 0; i < dani.length; i++) {
            if (dani[i].children.length != 0) {
                if (dani[i].children[0].classList.length != 0) {
                    if (dani[i].children[0].classList[0] == "zauzeta")
                        stringDrugi += "z";
                    else stringDrugi += "s";
                }
            }
        }
        assert.notEqual(stringPrvi, stringDrugi, "Sa podacima za isti dan mora svejedno biti obojena sala");
    });
});