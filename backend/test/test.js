var expect  = require('chai').expect;
var request = require('request');

it('Get Osobe response code', function(done) {
    request('http://localhost:3000/api/osoblje' , function(error, response, body) {
        
        expect(response.statusCode).to.equal(200);
        done();
    });
});


it('Get Pictures response code', function(done) {
    request('http://localhost:3000/api/pictures' , function(error, response, body) {
        
        expect(response.statusCode).to.equal(200);
        done();
    });
});

it('Get Rezervacija response code', function(done) {
    request('http://localhost:3000/api/rezervacija?from=11&to=12&sala=va' , function(error, response, body) {
        
        expect(response.statusCode).to.equal(200);
        done();
    });
});


it('Check existing', function(done) {
    request('http://localhost:3000/api/rezervacija?from=11&to=12&sala=va' , function(error, response, body) {
        
        expect(response).to.equal(0);
        done();
    });
});