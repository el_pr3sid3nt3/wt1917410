const http = require('http');
const fs = require('fs');

const hostname = '127.0.0.1';
const port = 3000;
const picturesFolderPath = __dirname + '/pictures';
//const zauzecaJson = 'zauzeca.json'; 
const db = require('./db.js');

var bufferedPictures = Array();
bufferPictures();
var bufferedZauzeca = Array();


const server = http.createServer((req, res) => {
	res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  res.setHeader('Access-Control-Allow-Credentials', true);
	if (req.url.indexOf('/api/') == 0 && req.url.length > 5) {
		let params = req.url.substring(5, req.url.length);
		router(params, res); 
	}

	
db.sequelize.sync({force:true}).then(async function(){

	await init();
	bufferZauzeca();
	console.log("Prosao...");

});

});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});

async function router(params, res) {
	let arr = getRouteFromURL(params);
	let from, to;
	switch (arr[0]) {
		case 'getRez':
				const rez = await bufferZauzeca();
				res.statusCode = 200;
				res.setHeader('Content-Type', 'application/json');
				res.write(JSON.stringify(rez));
				res.end();
				break;

		case 'osoblje':
				const  users = await db.Osoblje.findAll();
				res.statusCode = 200;
				res.setHeader('Content-Type', 'application/json');
				res.write(JSON.stringify(users));
				res.end();
				break;
		case 'pictures':
			for (let key in arr[1]) {
				if (key != 'from' && key != 'to') {
					return;
				}
			}
			from = parseInt(arr[1]['from']);
			to = parseInt(arr[1]['to']);
			if (from == null || to == null || from > to || from < 0) {
				res.end();
				return;
			}
			res.statusCode = 200;
			res.setHeader('Content-Type', 'application/json');
			let pictures = getPicturesFromBuffer(from, to);
			res.write(JSON.stringify(pictures));
			res.end();
  		break;
  	case 'rezervacija':
  		for (let key in arr[1]) {
				if (key != 'from' && key != 'to' && key != 'sala') {
					res.end();
					return;
				}
			}
			from = parseInt(arr[1]['from']);
			to = parseInt(arr[1]['to']);
			let sala = arr[1]['sala'];
			if (from == null || to == null || sala == null || from > to || from < 0) {
				res.end();
				return;
			}
			res.statusCode = 200;
			res.setHeader('Content-Type', 'application/json');
			let returnValue = checkZauzeca(from, to, sala);
			res.write(JSON.stringify(returnValue));
			res.end();
  		break;
  	default:
  		res.statusCode = 404;
  		res.setHeader('Content-Type', 'text/plain');
  		res.end();
	}
}

function getRouteFromURL(params) {
	let route = '';
	let urlParams = {};
	let urlParamsStart = 0;
	let char = '';
	for (let i = 0, n = params.length; i < n; i++) {
		char = params[i];
			//Parametri pocinju
		if (char == '?') {
			if (i + 3 < n) { // od '?' simbola moraju barem biti 3 simbola prisutna, jer je to najkraca moguca forma parametara, tipa : '?a=b' 
				urlParamsStart = i + 1; // +1 je dodan tako da dodjemo poslije '?' simbola u stringu
			}
			break;
		} else {
			route = route + char;
		}
	}
	if (urlParamsStart > 0) {
		let stringAfterQuestion = params.substring(urlParamsStart, params.length);
		let paramPairs = stringAfterQuestion.split('&');
		for (let i = 0, n = paramPairs.length; i < n; i++) {
			let arr = paramPairs[i].split('=');
			if (arr.length == 2) {
				urlParams[arr[0]] = arr[1];
			}
		}
	}
	return Array(route, urlParams);
}

function getPicturesFromBuffer(from, to) {
	let pictures = [];
	for (; from < to; from++) {
		if (from >= bufferedPictures.length) {
			pictures.push("0");
			break;
		}
		pictures.push(bufferedPictures[from]);
	}
	if (to == bufferedPictures.length) {
		pictures.push("0");
	}
	return pictures;
}

function checkZauzeca(from, to, sala) {
	for (let i = 0, n = bufferedZauzeca.length; i < n; i++) {
		if ((from >= bufferedZauzeca[i].from && from <= bufferedZauzeca[i].to && sala == bufferedZauzeca[i].sala) ||
				(to >= bufferedZauzeca[i].from && to <= bufferedZauzeca[i].to && sala == bufferedZauzeca[i].sala) ||
				(from < bufferedZauzeca[i].from && to > bufferedZauzeca[i].to && sala == bufferedZauzeca[i].sala)) {
			let response = Array();
			let fromDate = new Date(from);
			let toDate = new Date(to);
			response.push("0");
			response.push("Nije moguće rezervisati salu "+sala+" za navedeni datum: "+fromDate.getDate()+"/"+(fromDate.getMonth()+1)+"/"+fromDate.getFullYear()+
				" i termin od "+(fromDate.getHours()+1)+":"+(fromDate.getMinutes()+1)+" do "+(toDate.getHours()+1)+":"+(toDate.getMinutes()+1)+"!");
			return response;
		}
	}
	let newAppointment = {'from':from, 'to':to, 'sala':sala};
	bufferedZauzeca.push(newAppointment);
	let response = Array();
	response.push("1");
	for (let i = 0, n = bufferedZauzeca.length; i < n; i++) {
		response.push(bufferedZauzeca[i]);
	}
	saveZauzeca();
	return response;
}

function bufferPictures() {
	fs.readdir(picturesFolderPath, function(err, files) {
		if (err) {
			console.log("Folder za slike nije pronadjen. \n" + err);
			return;
		}

		files.forEach(function (file) {
			fs.readFile(picturesFolderPath + "/" + file, function(err, data) {
  			if (err) {
  				console.log("File se nije mogao ucitati. \n" + err);
  				throw err;
  			}
  			bufferedPictures.push(data);
  			console.log("Loaded picture: " + file);
  		});
		});
	});
}

async function bufferZauzeca() {
	// var data = fs.readFileSync('zauzeca.json');
	// console.log("zauzeca.json je ucitan.");
	// if (data.length == 0) return;
	// bufferedZauzeca = JSON.parse(data);
    
	let data =await db.Rezervacija.findAll();
	console.log(JSON.stringify(data));
	bufferedZauzeca = JSON.stringify(data);
	return bufferZauzeca;
}

async function saveZauzeca() {
	let array = []
	bufferZauzeca.forEach(async rez => {
		let rezer = await db.Rezervacija.create({termin: rez.termin, sala: rez.sala, osoba: rez.osoba});
		array.push(rezer);
	})

	return array;
}


const  init = async () => 
{
	const  user = await db.Osoblje.create({ ime: 'neko' , prezime: 'nekic', uloga: 'profesor'});
	const  user1 = await db.Osoblje.create({ ime: 'drugi' , prezime: 'neko', uloga: 'asistent'});
	const user2 = await db.Osoblje.create({ ime: 'Test' , prezime: 'Test', uloga: 'asistent'})

	const promiseArray = [];
	promiseArray.push(user);
	promiseArray.push(user1);
	promiseArray.push(user2);

	// Promise.all(promiseArray).then(user => console.log(user));

	const sala = await db.Sala.create({naziv: '1-11', zaduzenaOsoba: 1});
	const sala1 = await db.Sala.create({naziv: '1-15', zaduzenaOsoba: 2});
	 
	const promiseSalaArray = [];
	promiseSalaArray.push(sala);
	promiseSalaArray.push(sala1);

	// Promise.all(promiseSalaArray).then(user => console.log(user));

	const termin = await db.Termin.create({redovni:false, datum:'01.01.2020', pocetak: '12:00', kraj: '13:00'});
	const termin1 = await db.Termin.create({redovni:true, dan:0,semestar:"zimski",pocetak: '13:00', kraj: '14:00'});

	const promiseTerminArray= [];
	promiseTerminArray.push(termin);
	promiseTerminArray.push(termin1);
	// Promise.all(promiseTerminArray).then(termin => console.log(termin));

	const rezervacija = await db.Rezervacija.create({termin: 1, sala: 1, osoba: 1});
	const rezervacija1 = await db.Rezervacija.create({termin: 2, sala: 1, osoba: 3});

	const promiseRezervacijaArray = [];
	promiseRezervacijaArray.push(rezervacija);
	promiseRezervacijaArray.push(rezervacija1);

	// Promise.all(promiseRezervacijaArray).then(rez => console.log(rez));
}