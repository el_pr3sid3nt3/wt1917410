const Sequelize = require("sequelize");
const sequelize = new Sequelize("DBWT19","root","root",{host:"127.0.0.1",dialect:"mysql",logging:false,
define: {
    timestamps: false,
    freezeTableName: true
      
}});
const db={};

db.Sequelize = Sequelize;  
db.sequelize = sequelize;

//import modela
db.Osoblje = sequelize.import(__dirname+'/models/Osoblje.js');
db.Rezervacija = sequelize.import(__dirname+'/models/Rezervacija.js');
db.Sala = sequelize.import(__dirname+'/models/Sala.js');
db.Termin = sequelize.import(__dirname+'/models/Termin.js')


//relacijeforeignKey
// // Veza 1-n vise knjiga se moze nalaziti u biblioteci
db.Osoblje.hasMany(db.Rezervacija,{foreignKey: 'osoba'});
db.Sala.hasMany(db.Rezervacija,{foreignKey: 'sala'});
db.Termin.hasOne(db.Rezervacija,{foreignKey: 'termin'});
db.Osoblje.hasOne(db.Sala,{foreignKey: 'zaduzenaOsoba'});



module.exports=db;

