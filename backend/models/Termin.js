const Sequelize = require("sequelize");

module.exports = function(sequelize, DataTypes){
    const Termin = sequelize.define("Termin",{
        redovni: Sequelize.BOOLEAN,
        dan: Sequelize.INTEGER,
        datum: Sequelize.DATE,
        semestar: Sequelize.STRING,
        pocetak: Sequelize.TIME,
        kraj: Sequelize.TIME
    },{
        freezeTableName: true
      })
    return Termin;
};