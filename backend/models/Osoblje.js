const Sequelize = require("sequelize");

module.exports = function(sequelize, DataTypes){
    const Osoblje = sequelize.define("Osoblje",{
        ime: Sequelize.TEXT,
        prezime: Sequelize.TEXT,
                uloga: Sequelize.TEXT
    },{
        freezeTableName: true
      })
    return Osoblje;
};